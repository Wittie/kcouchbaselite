package namics.tdojo.trycouchbase.data.database

import android.support.test.InstrumentationRegistry
import com.couchbase.lite.CouchbaseLiteException
import junit.framework.Assert.*
import namics.tdojo.trycouchbase.TryCouchBaseApplication
import org.junit.*
import java.util.*

class CouchBaseDBHandlerTest {

    companion object {

        private val KEY_GREETING = "KEY_GREETING"
        private val KEY_FAREWELL = "KEY_FAREWELL"

        lateinit var couchBaseDBHandler: CouchBaseDBHandler

        @BeforeClass
        @JvmStatic
        fun setup() {
            // things to execute once and keep around for the class
            val appContext = InstrumentationRegistry.getTargetContext().applicationContext
            couchBaseDBHandler = CouchBaseDBHandler(appContext as TryCouchBaseApplication)
        }

        @AfterClass
        @JvmStatic
        fun teardown() {
            // clean up after this class, leave nothing dirty behind
            couchBaseDBHandler.database.close()
        }
    }

    @After
    fun afterTest() {
        // Delete all documents one by one in order not to delete and close the whole database
        couchBaseDBHandler.database.createAllDocumentsQuery().run().map { it.document }
                .forEach {
                    couchBaseDBHandler.database.getExistingDocument(it.id)?.delete()
                }
    }

    //region HELPER FUNCTIONS

    private fun createListOfHashMap(numberOfElements: Int): List<HashMap<String, Any>> {
        return List (numberOfElements) {
            hashMapOf(KEY_GREETING to "hola, $it" as Any, KEY_FAREWELL to "Adios, $it")
        }
    }

    private fun createRandomDocuments(numberOfElements: Int) {
        createListOfHashMap(numberOfElements).forEach {
            couchBaseDBHandler.saveOrCreateDocument(UUID.randomUUID().toString(), CouchBaseDBHandler.DOC_TYPE, it)
        }
    }

    //endregion

    @Test
    @Throws(CouchbaseLiteException::class)
    fun size_OnEmptyDataBase_shouldReturnZero() {
        assertEquals(couchBaseDBHandler.size(), 0)
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun size_OnNonEmptyDataBase_shouldReturnProperNumberOfDocuments() {
        val numberOfElementsToCreate = 10
        createRandomDocuments(numberOfElementsToCreate)
        assertEquals(numberOfElementsToCreate, couchBaseDBHandler.size())
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun getAllDocuments_OnEmptyDataBase_shouldReturnAnEmptyQueryEnumerator() {
        assertNotNull(couchBaseDBHandler.getAllDocuments())
        assertEquals(0, couchBaseDBHandler.getAllDocuments()!!.count)
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun getAllDocuments_OnNonEmptyDataBase_shouldReturnProperDocuments() {
        val numberOfElementsToCreate = 10
        createRandomDocuments(numberOfElementsToCreate)
        val list = couchBaseDBHandler.getAllDocuments()?.map { it.document }
        assertNotNull(list)
        assertEquals(numberOfElementsToCreate, list?.size)
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun delete_OnEmptyDataBase_shouldDoNothing() {
        val nonExistentId = UUID.randomUUID().toString()
        couchBaseDBHandler.delete(nonExistentId)
        assertEquals(0, couchBaseDBHandler.size())
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun delete_OnNonEmptyDataBase_shouldDeleteProperly() {
        val listOfHashMap = createListOfHashMap(2)
        val randomId1 = UUID.randomUUID().toString()
        val randomId2 = UUID.randomUUID().toString()
        couchBaseDBHandler.saveOrCreateDocument(randomId1, CouchBaseDBHandler.DOC_TYPE, listOfHashMap[0])
        couchBaseDBHandler.saveOrCreateDocument(randomId2, CouchBaseDBHandler.DOC_TYPE, listOfHashMap[1])
        assertEquals(2, couchBaseDBHandler.size())
        couchBaseDBHandler.delete(randomId1)
        assertEquals(1, couchBaseDBHandler.size())
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun deleteAllDocuments_OnEmptyDataBase_shouldDoNothing() {
        couchBaseDBHandler.deleteAllDocuments()
        assertEquals(0, couchBaseDBHandler.size())
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun deleteAllDocuments_OnNonEmptyDataBase_shouldDeleteEverything() {
        val numberOfElementsToCreate = 10
        createRandomDocuments(numberOfElementsToCreate)
        couchBaseDBHandler.deleteAllDocuments()
        assertEquals(0, couchBaseDBHandler.size())
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun getOrCreateDocument_onExistingDocument_shouldGetDocument() {
        val listOfHashMap = createListOfHashMap(1)
        val randomId1 = UUID.randomUUID().toString()
        couchBaseDBHandler.saveOrCreateDocument(randomId1, CouchBaseDBHandler.DOC_TYPE, listOfHashMap[0])
        val existingDocument = couchBaseDBHandler.getOrCreateDocument(randomId1)
        assertNotNull(existingDocument)
        assertNotNull(existingDocument.currentRevision)
        assertEquals(randomId1, existingDocument.id)
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun getOrCreateDocument_onNewDocument_shouldGetNonSavedDocument() {
        val randomId1 = UUID.randomUUID().toString()
        val existingDocument = couchBaseDBHandler.getOrCreateDocument(randomId1)
        assertEquals(0, couchBaseDBHandler.size())
        assertNotNull(existingDocument)
        assertNull(existingDocument.currentRevision)
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun updateDocument_onExistingDocument_shouldUpdateDocument() {

        val originalId = UUID.randomUUID().toString()
        val listOfHashMap = createListOfHashMap(2)

        couchBaseDBHandler.saveOrCreateDocument(originalId, CouchBaseDBHandler.DOC_TYPE, listOfHashMap[0])

        val retrievedDocument = couchBaseDBHandler.getOrCreateDocument(originalId)
        val isSaved = couchBaseDBHandler.updateDocument(retrievedDocument, listOfHashMap[1])

        assertEquals(1, couchBaseDBHandler.size())
        assertEquals(originalId, retrievedDocument.id)
        assertEquals(listOfHashMap[1][KEY_GREETING], retrievedDocument.properties[KEY_GREETING])
        assertEquals(listOfHashMap[1][KEY_FAREWELL], retrievedDocument.properties[KEY_FAREWELL])
        assertTrue(isSaved)
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun updateDocument_onNonExistingDocument_TheDocumentIsSaved() {

        val newId = UUID.randomUUID().toString()
        val listOfHashMap = createListOfHashMap(1)
        val newDocument = couchBaseDBHandler.getOrCreateDocument(newId)
        val isSaved = couchBaseDBHandler.updateDocument(newDocument, listOfHashMap[0])

        val retrievedDocument = couchBaseDBHandler.getOrCreateDocument(newId)

        assertEquals(1, couchBaseDBHandler.size())
        assertNotNull(newDocument)
        assertNotNull(newDocument.currentRevision)
        assertEquals(listOfHashMap[0][KEY_GREETING], retrievedDocument.properties[KEY_GREETING])
        assertEquals(listOfHashMap[0][KEY_FAREWELL], retrievedDocument.properties[KEY_FAREWELL])
        assertTrue(isSaved)
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun createCouchBasePropertyView_and_getCouchBaseView_shouldFilterProperly() {

        val numberOfElementsToCreate = 20
        val listOfHashMaps = createListOfHashMap(numberOfElementsToCreate)
        for (hashMap in listOfHashMaps) {
            couchBaseDBHandler.saveOrCreateDocument(UUID.randomUUID().toString(), CouchBaseDBHandler.DOC_TYPE, hashMap)
        }

        val viewName = "filterByGreeting"
        val recalculateView = true
        val couchBasePropertyView = couchBaseDBHandler.createCouchBasePropertyView<String>(viewName, KEY_GREETING, recalculateView) {            it.startsWith("hola, 1", ignoreCase = true)
        }

        val filteredList = couchBaseDBHandler.runQueryForView(couchBasePropertyView)

        assertEquals(11, filteredList.size)
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun createCouchBasePropertyView_and_getCouchBaseView_shouldBeEmptyWhenAllIsFiltered() {
        val numberOfElementsToCreate = 20
        createRandomDocuments(numberOfElementsToCreate)

        val viewName = "filterByGreeting"
        val recalculateView = true
        val couchBasePropertyView = couchBaseDBHandler.createCouchBasePropertyView<String>(viewName, KEY_GREETING, recalculateView) {
            it.startsWith("hallo", ignoreCase = true)
        }

        val filteredList = couchBaseDBHandler.runQueryForView(couchBasePropertyView)

        assertEquals(0, filteredList.size)
    }


    ///////////////// TEST FOR COUCH DB OBJECT TO SEE ITS BEHAVIOUR


    @Test
    @Throws(CouchbaseLiteException::class)
    fun getExistingDocument_OnNonExistingDocument_shouldReturnNull() {
        val existingDocument = couchBaseDBHandler.database.getExistingDocument(UUID.randomUUID().toString())
        Assert.assertNull(existingDocument)
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun getExistingDocument_OnExistingDocument_shouldReturnDocument() {
        val listOfHashMaps = createListOfHashMap(1)
        val id = UUID.randomUUID().toString()
        couchBaseDBHandler.saveOrCreateDocument(id, CouchBaseDBHandler.DOC_TYPE, listOfHashMaps[0])
        val document = couchBaseDBHandler.getOrCreateDocument(id)

        val existingDocument = couchBaseDBHandler.database.getExistingDocument(document.id)
        Assert.assertNotNull(existingDocument)
        Assert.assertNotNull(existingDocument.currentRevision)
        Assert.assertEquals(document, existingDocument)
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun getDocument_toCreateDocument_withNoProperties_shouldCreateNoDocument() {
        val id = UUID.randomUUID().toString()
        val document = couchBaseDBHandler.database.getDocument(id)
        Assert.assertNull(document.currentRevision)

        val existingDocument = couchBaseDBHandler.database.getExistingDocument(id)
        Assert.assertNull(existingDocument)
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun getDocument_toCreateDocument_withProperties_shouldCreateDocument() {
        val id = UUID.randomUUID().toString()
        val document = couchBaseDBHandler.database.getDocument(id)
        document.putProperties(emptyMap())
        Assert.assertNotNull(document.currentRevision)

        val existingDocument = couchBaseDBHandler.database.getExistingDocument(id)
        Assert.assertNotNull(existingDocument)
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun getDocument_toRetrieveDocument_OnNonExistingDocument_shouldReturnNonSavedDocument() {
        val document = couchBaseDBHandler.database.getDocument(UUID.randomUUID().toString())
        Assert.assertNotNull(document)
        Assert.assertNull(document.currentRevision)
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun getDocument_toRetrieveDocument_OnExistingDocument_shouldRetrieveDocument() {
        val listOfHashMaps = createListOfHashMap(1)
        val id = UUID.randomUUID().toString()
        couchBaseDBHandler.saveOrCreateDocument(id, CouchBaseDBHandler.DOC_TYPE, listOfHashMaps[0])
        val document = couchBaseDBHandler.getOrCreateDocument(id)

        val existingDocument = couchBaseDBHandler.database.getDocument(document.id)
        Assert.assertNotNull(document)
        Assert.assertNotNull(document.currentRevision)
        Assert.assertEquals(document, existingDocument)
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun deleteDocument_OnExistingDocument_shouldDeleteDocument() {
        val listOfHashMaps = createListOfHashMap(1)
        val id = UUID.randomUUID().toString()
        couchBaseDBHandler.saveOrCreateDocument(id, CouchBaseDBHandler.DOC_TYPE, listOfHashMaps[0])
        val document = couchBaseDBHandler.getOrCreateDocument(id)

        Assert.assertEquals(1, couchBaseDBHandler.size())
        document.delete()
        Assert.assertEquals(0, couchBaseDBHandler.size())

    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun deleteDocument_OnNonExistingDocument_shouldDoNothing() {
        val document = couchBaseDBHandler.database.getDocument(UUID.randomUUID().toString())
        document.delete()
        Assert.assertEquals(0, couchBaseDBHandler.size())
    }

}


