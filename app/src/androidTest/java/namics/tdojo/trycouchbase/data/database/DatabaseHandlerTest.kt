package namics.tdojo.trycouchbase.data.database

import android.support.test.InstrumentationRegistry
import com.couchbase.lite.CouchbaseLiteException
import com.couchbase.lite.Document
import namics.tdojo.trycouchbase.TryCouchBaseApplication
import namics.tdojo.trycouchbase.data.Team
import namics.tdojo.trycouchbase.data.TeamGenerator
import namics.tdojo.trycouchbase.data.UserInfo
import org.junit.*
import org.junit.Assert.*
import java.util.*

class DatabaseHandlerTest {

    companion object {

        lateinit var databaseHandler: DatabaseHandler

        val numberOfImportedTeams = 8

        @BeforeClass
        @JvmStatic
        fun setup() {
            // things to execute once and keep around for the class
            val appContext = InstrumentationRegistry.getTargetContext().applicationContext
            databaseHandler = DatabaseHandler(appContext as TryCouchBaseApplication)
        }

        @AfterClass
        @JvmStatic
        fun teardown() {
            // clean up after this class, leave nothing dirty behind
            databaseHandler.database.close()
        }
    }

    @Before
    fun before() {
        assertEquals(numberOfImportedTeams, databaseHandler.numberOfTeams)
    }

    @After
    fun after() {
        databaseHandler.deleteAllDocuments()
        databaseHandler.fakeFirstSyncIf(true)
        assertEquals(numberOfImportedTeams, databaseHandler.numberOfTeams)
    }

    //region HELPER FUNCTIONS

    @Throws(CouchbaseLiteException::class)
    fun getRandomExistingDocument(): Document {
        val fullList = databaseHandler.database.createAllDocumentsQuery().run().map { it.document }
        val size = fullList.size
        return if (size > 0) {
            fullList[Random().nextInt(size)]
        }  else throw Exception("Empty Database when it shouldn't")
    }

    //endregion



    @Test
    fun areCouchBaseViewsSet_byDefault_noneOfThemShouldBeNull() {
        assertNotNull(databaseHandler.userInfoDocTypeView)
        assertNotNull(databaseHandler.teamDocTypeView)
        assertNotNull(databaseHandler.searchInTeamsDocTypeView)
        assertNotNull(databaseHandler.searchPrefix)
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun importDefaultTeams_importDefaultTeams_properly() {
        assertEquals(numberOfImportedTeams, databaseHandler.numberOfTeams)
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun importDefaultUserInfo_generateDefaultUserInfo_properly() {
        assertNotNull(databaseHandler.getUserInfo())
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun saveUserInfo_enEmptyDB_shouldReturnNull() {
        databaseHandler.deleteAllDocuments()
        assertNull(databaseHandler.getUserInfo())
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun saveUserInfo_shouldOverwritePreviousUserInfo() {
        val userInfo1 = UserInfo(name = "Name1", imageResId = 12345)
        databaseHandler.saveUserInfo(userInfo1)

        assertNotNull(databaseHandler.getUserInfo())
        assertEquals(userInfo1, databaseHandler.getUserInfo())
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun saveTeam_onNewTeam_shouldCreateNewDocument() {
        databaseHandler.saveTeam(TeamGenerator.generate())
        assertEquals(numberOfImportedTeams + 1, databaseHandler.numberOfTeams)
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun saveTeam_onExistingTeam_shouldUpdateTeam() {
        val existingTeamDocument = getRandomExistingDocument()
        assertNotNull(existingTeamDocument)

        val existingTeam = Team.fromDocument(existingTeamDocument)
        val newOldTeam = Team(id = existingTeam.id,
                name = existingTeam.name,
                championsLeague = -1,
                foundedAtString = existingTeam.foundedAtString,
                selected = existingTeam.selected)

        databaseHandler.saveTeam(newOldTeam)

        val retrievedTeam = Team.fromDocument(databaseHandler.getOrCreateDocument(existingTeam.id))

        assertEquals(newOldTeam.id, retrievedTeam.id)
        assertEquals(newOldTeam.name, retrievedTeam.name)
        assertEquals(newOldTeam.championsLeague, retrievedTeam.championsLeague)
        assertEquals(newOldTeam.foundedAtString, retrievedTeam.foundedAtString)
        assertEquals(newOldTeam.selected, retrievedTeam.selected)
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun createNewDocuments_shouldCreateNewDocuments() {
        val numberOfTeamsToCreate = 10
        val newTeams = List (numberOfTeamsToCreate) { TeamGenerator.generate() }

        databaseHandler.createNewDocuments(newTeams)
        assertEquals(numberOfImportedTeams + numberOfTeamsToCreate, databaseHandler.numberOfTeams)
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun deleteTeam_onExistingTeam_shouldDeleteDocument() {
        val team1 = TeamGenerator.generate()
        val team2 = TeamGenerator.generate()
        databaseHandler.saveTeam(team1)
        databaseHandler.saveTeam(team2)
        val newNumberOfDocuments = databaseHandler.numberOfTeams
        val randomExistingTeam = Team.fromDocument(getRandomExistingDocument())
        databaseHandler.deleteTeam(randomExistingTeam)
        assertEquals(newNumberOfDocuments - 1, databaseHandler.numberOfTeams)
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun deleteTeam_onNewTeam_shouldDoNothing() {
        val newTeam = TeamGenerator.generate()
        databaseHandler.deleteTeam(newTeam)
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun getFullTeamList_OnEmptyDataBase_shouldReturnProperNumberOfDocuments() {
        assertEquals(numberOfImportedTeams, databaseHandler.getFullTeamList().size)
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun getFullTeamList_OnNonEmptyDataBase_shouldReturnProperNumberOfDocuments() {
        val teamsToCreate = 10
        val newTeams = List(teamsToCreate){ TeamGenerator.generate() }
        databaseHandler.createNewDocuments(newTeams)

        assertEquals(numberOfImportedTeams + teamsToCreate, databaseHandler.getFullTeamList().size)
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun getTeamFilteredByName_OnEmptyDataBase_shouldReturnEmptyList() {
        val list = databaseHandler.getFullTeamList()
        list.forEach { databaseHandler.deleteTeam(it) }

        assertEquals(0, databaseHandler.getTeamFilteredByName("real").size)
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun getTeamFilteredByName_OnNonEmptyDataBase_shouldReturnRightList() {
        assertEquals(1, databaseHandler.getTeamFilteredByName("real").size)
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun getTeamFilteredByName_OnNonEmptyDataBase_shouldReturnEmptyIfNotFound() {
        assertEquals(0, databaseHandler.getTeamFilteredByName("zzz").size)
    }

    @Test
    @Throws(CouchbaseLiteException::class)
    fun getTeamFilteredByName_shouldFilterTeamsProperly() {
        val prefix = "real"
        val numberOfTeamsToCreate = 50
        val newTeams = List (numberOfTeamsToCreate) { TeamGenerator.generate() }
        val numberOfNewFilteredTeams = newTeams.count { it.name.startsWith(prefix, true) }
        val numberOfOldFilteredTeams = databaseHandler.getFullTeamList().count { it.name.startsWith(prefix, true) }
        val expectedValue = numberOfOldFilteredTeams + numberOfNewFilteredTeams

        newTeams.forEach { databaseHandler.saveTeam(it) }
        val retrievedValue = databaseHandler.getTeamFilteredByName(prefix).count()
        assertEquals(expectedValue, retrievedValue)
    }

}