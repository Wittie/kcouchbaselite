package namics.tdojo.trycouchbase.data

import android.support.annotation.DrawableRes
import com.couchbase.lite.Document
import namics.tdojo.trycouchbase.R
import java.util.*

data class UserInfo(val name: String,
                    @DrawableRes val imageResId: Int = R.drawable.ic_person_black_96dp) {

    val id = "user_info_unique_id"

    companion object {
        fun fromDocument(document: Document?): UserInfo? {
            return document?.let {
                with(document) {
                    UserInfo(name = getProperty("name") as String,
                            imageResId = getProperty("imageResId") as Int
                    )
                }
            }
        }
    }

    fun toHashMap(): HashMap<String, Any> {
        val hashMap = HashMap<String, Any>()
        hashMap.put("name", name)
        hashMap.put("imageResId", imageResId)
        return hashMap
    }
}


