package namics.tdojo.trycouchbase.data

import namics.tdojo.trycouchbase.R
import java.util.*

class UserInfoGenerator {

    companion object {

        fun generate() = UserInfo(name = "Mickey Maus ${Random().nextInt(100)}", imageResId = R.drawable.ic_mouse_black_96dp)

    }
}