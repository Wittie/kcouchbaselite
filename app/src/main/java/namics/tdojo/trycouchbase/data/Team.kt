package namics.tdojo.trycouchbase.data

import com.couchbase.lite.Document
import java.util.*

class Team constructor(val id : String = UUID.randomUUID().toString(),
           val name : String,
           val championsLeague : Int,
           val foundedAtString : String,
           var selected : Boolean = false) : Comparable<Team> {

    companion object {

        val FIELD_NAME = "name"
        val FIELD_CHAMPIONS = "championsLeague"
        val FIELD_FOUNDATION = "foundedAtString"
        val FIELD_SELECTED = "selected"

        fun fromDocument(document: Document): Team {
            return with(document) {
                Team(
                    id = document.id,
                    name = getProperty(FIELD_NAME) as String,
                    championsLeague = getProperty(FIELD_CHAMPIONS) as Int,
                    foundedAtString = getProperty(FIELD_FOUNDATION) as String,
                    selected = getProperty(FIELD_SELECTED) as Boolean
                )
            }
        }
    }

    override fun compareTo(other: Team): Int {
        val compareTo = name.compareTo(other.name)
        return if (compareTo != 0) compareTo else championsLeague.compareTo(other.championsLeague)
    }

    fun toHashMap(): HashMap<String, Any> {
        val hashMap = HashMap<String, Any>()
        hashMap.put(FIELD_NAME, name)
        hashMap.put(FIELD_CHAMPIONS, championsLeague)
        hashMap.put(FIELD_FOUNDATION, foundedAtString)
        hashMap.put(FIELD_SELECTED, selected)
        return hashMap
    }

}