package namics.tdojo.trycouchbase.data.database

import android.util.Log
import com.couchbase.lite.Document
import com.couchbase.lite.View
import namics.tdojo.trycouchbase.TryCouchBaseApplication
import namics.tdojo.trycouchbase.data.Team
import namics.tdojo.trycouchbase.data.UserInfo
import namics.tdojo.trycouchbase.data.UserInfoGenerator


class DatabaseHandler(application: TryCouchBaseApplication) : CouchBaseDBHandler(application) {

    val TAG: String = DatabaseHandler::class.java.name

    private val TEAM_NAME_STARTS_WITH_VIEW = "TeamNameStartsWithView"
    private val ALL_TEAMS_VIEW = "AllTeamsView"
    private val USER_INFO_VIEW = "UserInfoView"

    internal lateinit var userInfoDocTypeView : View
    internal lateinit var teamDocTypeView : View

    internal lateinit var searchInTeamsDocTypeView : View
    internal var searchPrefix = ""

    var numberOfTeams: Int = 0
        get() {
            return teamDocTypeView.createQuery().run().count
        }

    init {
        updateCouchBaseViews()
        fakeFirstSyncIf(numberOfTeams == 0)
    }

    fun updateCouchBaseViews() {
        calculateUserInfoView()
        calculateTeamView()
        calculateSearchTeamsView()
    }

    fun calculateUserInfoView() {
        Log.d(TAG, "calculateUserInfoView")
        userInfoDocTypeView = createCouchBasePropertyView<String>(USER_INFO_VIEW, DOC_TYPE) {
            it == UserInfo::class.java.simpleName
        }
    }

    fun calculateTeamView() {
        Log.d(TAG, "calculateTeamView")
        teamDocTypeView = createCouchBasePropertyView<String>(ALL_TEAMS_VIEW, DOC_TYPE) {
            it == Team::class.java.simpleName
        }
    }

    fun calculateSearchTeamsView(recalculateView: Boolean = false) {
            Log.d(TAG, "calculateSearchTeamView")
        searchInTeamsDocTypeView = createCouchBasePropertyView<String>(TEAM_NAME_STARTS_WITH_VIEW, Team.FIELD_NAME, recalculateView = recalculateView) {
            it.startsWith(prefix = searchPrefix, ignoreCase = true)
        }
    }

    internal fun fakeFirstSyncIf(shouldAddTeams: Boolean) {
        if (shouldAddTeams) {
            Log.d(TAG, "Generating List Teams...")
            val importTeams = importTeams()
            importTeams.forEach { saveTeam(it) }

            val userInfo = UserInfoGenerator.generate()
            saveUserInfo(userInfo)

        } else {
            Log.d(TAG, "No teams generated")
        }
    }

    fun saveUserInfo(userInfo: UserInfo): Boolean {
        val document: Document? = database.getExistingDocument(userInfo.id)
        if (document == null) {
            return saveOrCreateDocument(userInfo.id, UserInfo::class.java.simpleName, userInfo.toHashMap())
        } else {
            return updateDocument(document, userInfo.toHashMap())
        }
    }

    // Updates/saves new Team into database
    fun saveTeam(team: Team): Boolean {

        val document: Document? = database.getExistingDocument(team.id)

        if (document == null) {
            return saveOrCreateDocument(team.id, Team::class.java.simpleName, team.toHashMap())
        } else {
            return updateDocument(document, team.toHashMap())
        }
    }

    fun createNewDocuments(newTeams: List<Team>) {
        for (team in newTeams) {
            saveOrCreateDocument(team.id, Team::class.java.simpleName, team.toHashMap())
        }
    }

    fun deleteTeam(team: Team) = delete(team.id)

    fun importTeams(): List<Team> {
        return listOf(
                Team(name = "Real Madrid", championsLeague = 12, foundedAtString = "06.03.1902"),
                Team(name = "Milan", championsLeague = 7, foundedAtString = "13.12.1899"),
                Team(name = "Bayern Munich", championsLeague = 5, foundedAtString = "27.02.1900"),
                Team(name = "Barcelona", championsLeague = 5, foundedAtString = "29.11.1899"),
                Team(name = "Liverpool", championsLeague = 5, foundedAtString = "03.06.1892"),

                Team(name = "Ajax", championsLeague = 4, foundedAtString = "18.03.1900"),
                Team(name = "Manchester United", championsLeague = 3, foundedAtString = "01.01.1902"),
                Team(name = "Atlético de Madrid", championsLeague = 0, foundedAtString = "26.04.1903")
        )
    }

    fun getFullTeamList(): List<Team> {
        return runQueryForView(teamDocTypeView).map { Team.fromDocument(it) }
    }

    fun getTeamFilteredByName(prefix: String) : List<Team> {
        if (prefix != searchPrefix) {
            searchPrefix = prefix
            calculateSearchTeamsView(recalculateView = true)
        }

        return runQueryForView(searchInTeamsDocTypeView).map { Team.fromDocument(it) }
    }

    fun getUserInfo(): UserInfo? {
        return UserInfo.fromDocument(runQueryForView(userInfoDocTypeView).firstOrNull())
    }

    fun addChangeListenerToUserInfoDocument(changeListener: Document.ChangeListener) {
        val userInfoDocument = runQueryForView(userInfoDocTypeView).firstOrNull()
        userInfoDocument?.addChangeListener(changeListener)
    }

    fun setUserName(newName : String): Unit {
        val userInfoDocument = runQueryForView(userInfoDocTypeView).firstOrNull()
        if (userInfoDocument != null) {
            updateDocument(userInfoDocument, hashMapOf("name" to newName))
        } else {
            val newUserInfo = UserInfo(name = newName)
            saveUserInfo(newUserInfo)
        }
    }

    /* SLOW WAY, NOT WORTH IT
    fun getTeamsWhoseNameStartsWith(nameStart: String): List<Team> {

        val startTime = System.nanoTime()
        val queryEnumerator = database.createAllDocumentsQuery().run()
        val timeRequired = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startTime)
        Log.d(TAG, "Time consumed in run query for Teams' Name: $timeRequired ms")

        return queryEnumerator
                .filter {
                    val nameString = it.document.getProperty("name") as String
                    nameString.startsWith(prefix = nameStart, ignoreCase = true)
                }
                .map { Team.fromDocument(it.document) }
    }
    */
}
