package namics.tdojo.trycouchbase.data

import java.util.*

class TeamGenerator {

    companion object {

        fun generate() = Team(name = "${getRandomFirstFootballClubName()} ${getRandomSecondFootballClubName()}",
                championsLeague = Random().nextInt(12),
                foundedAtString = getRandomStringDate()
        )

        private fun getRandomStringDate() = "${Random().nextInt(28) + 1}.${Random().nextInt(12) + 1}.${Random().nextInt(100) + 1899}"

        private fun getRandomFirstFootballClubName() = when (Random().nextInt(16)) {
            0 -> "Real"
            1 -> "Sporting"
            2 -> "Bayern"
            3 -> "Inter"
            4 -> "Borusia"
            5 -> "Rapid"
            6 -> "Racing"
            7 -> "Sant"
            8 -> "Athletic"
            9 -> "Atlético"
            10 -> "Royal"
            11 -> "Partizan"
            12 -> "Dinamo"
            13 -> "Standard"
            14 -> "Club"
            else -> "Football Club"
        }

        private fun getRandomSecondFootballClubName() = when (Random().nextInt(16)) {
            0 -> "Arschloch"
            1 -> "Tapas"
            2 -> "Siesta"
            3 -> "Joder"
            4 -> "Cabron"
            5 -> "Bielefeld"
            6 -> "Offenbach"
            7 -> "Kissing"
            8 -> "Fucking"
            9 -> "Mist"
            10 -> "Scheiße"
            11 -> "Kacke"
            12 -> "Quatsch"
            13 -> "Mierda"
            14 -> "Shitty"
            else -> "Crappy"
        }

    }
}