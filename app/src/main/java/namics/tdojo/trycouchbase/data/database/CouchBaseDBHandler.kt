package namics.tdojo.trycouchbase.data.database

import android.util.Log
import com.couchbase.lite.*
import namics.tdojo.trycouchbase.TryCouchBaseApplication
import java.util.concurrent.TimeUnit

open class CouchBaseDBHandler(application: TryCouchBaseApplication) {

    private val TAG : String = CouchBaseDBHandler::class.java.name

    companion object { val DOC_TYPE = "docType" }

    val database: Database = application.database

    fun size() = database.createAllDocumentsQuery().run().count

    @Suppress("unused")
    fun getAllDocuments() : QueryEnumerator? = database.createAllDocumentsQuery().run()

    fun delete(id: String) = database.getExistingDocument(id)?.delete()

    fun deleteAllDocuments() = database.createAllDocumentsQuery().run().forEach { it.document.delete() }

    fun getOrCreateDocument(id: String) : Document = database.getDocument(id)

    fun saveOrCreateDocument(id: String, docType: String, attributes: HashMap<String, Any>): Boolean {
        val document = getOrCreateDocument(id)
        try {
            attributes.put(DOC_TYPE, docType)
            document.putProperties(attributes)
        } catch (e: CouchbaseLiteException) {
            Log.e(TAG, "Cannot saveTeam document", e)
            return false
        }

        return true
    }

    fun updateDocument(document: Document, attributes: HashMap<String, Any>): Boolean {
        val update = document.createRevision()
        val properties = update.properties
        properties.putAll(attributes)

        try {
            update.save()
        } catch (ex: CouchbaseLiteException) {
            Log.e(TAG, "CBL operation failed")
            return false
        }

        return true
    }

    fun runQueryForView(view: View): List<Document> {
        return view.createQuery().run().map { it.document }
    }

    fun <PropertyValueClass>createCouchBasePropertyView(viewName: String,
                                          propertyKey : String,
                                          recalculateView: Boolean = false,
                                          predicate: ((PropertyValueClass) -> Boolean)) : View {
        val nameView = database.getView(viewName)

        if (nameView.mapVersion == null || recalculateView) {

            val startTime = System.nanoTime()

            val version = nameView.mapVersion?.let { (nameView.mapVersion.toInt() + 1).toString() } ?: "0"


            val mapBlock = Mapper { documentAttributes, emitter ->
                @Suppress("UNCHECKED_CAST")
                val propertyValue = documentAttributes[propertyKey] as PropertyValueClass
                if (predicate(propertyValue)) {
                    emitter.emit(propertyValue, documentAttributes[propertyKey])
                }
            }

            nameView.setMap(mapBlock, version)

            val timeRequired = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startTime)
            Log.d(TAG, "Creating $viewName CoachView for: $propertyKey version $version in $timeRequired ms")
        }

        return nameView
    }
}
