package namics.tdojo.trycouchbase.view.custom_views

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.widget.Button
import android.widget.NumberPicker
import namics.tdojo.trycouchbase.R
import namics.tdojo.trycouchbase.view.bind

class NumberPickerDialog : Dialog {

    constructor(context: Context) : super(context)
    constructor(context: Context, themeResId: Int) : super(context, themeResId)
    constructor(context: Context, cancelable: Boolean, cancelListener: DialogInterface.OnCancelListener?) : super(context, cancelable, cancelListener)

    val button: Button by bind(R.id.dialog_picker_button)
    val thousandNumberPicker: NumberPicker by bind(R.id.dialog_picker_number_thousands)
    val hundredsNumberPicker: NumberPicker by bind(R.id.dialog_picker_number_hundreds)
    val tensNumberPicker: NumberPicker by bind(R.id.dialog_picker_number_tens)
    val unitsNumberPicker: NumberPicker by bind(R.id.dialog_picker_number_units)

    var onNumberPicked: ((Int) -> Unit)? = null

    init {
        setTitle("Number Picker")
        setContentView(R.layout.dialog_picker_number)

        setNumberPicker(thousandNumberPicker)
        setNumberPicker(hundredsNumberPicker)
        setNumberPicker(tensNumberPicker)
        setNumberPicker(unitsNumberPicker)

        button.setOnClickListener {
            val numberPicked = thousandNumberPicker.value * 1000 +
                    hundredsNumberPicker.value * 100 +
                    tensNumberPicker.value * 10 +
                    unitsNumberPicker.value
            onNumberPicked?.invoke(numberPicked)
            hide()
        }
    }

    private fun setNumberPicker(thousandNumberPicker: NumberPicker) {
        thousandNumberPicker.maxValue = 9
        thousandNumberPicker.minValue = 0
        thousandNumberPicker.wrapSelectorWheel = true
    }




}