package namics.tdojo.trycouchbase.view

import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import namics.tdojo.trycouchbase.R
import namics.tdojo.trycouchbase.TryCouchBaseApplication
import namics.tdojo.trycouchbase.data.Team
import namics.tdojo.trycouchbase.data.TeamGenerator
import namics.tdojo.trycouchbase.data.database.DatabaseHandler
import namics.tdojo.trycouchbase.view.custom_views.NumberPickerDialog
import namics.tdojo.trycouchbase.view.custom_views.TextBoxDialog
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.concurrent.TimeUnit


class MainActivity : AppCompatActivity() {

    private val TAG : String = this::class.java.name

    private val recyclerView: RecyclerView by bind(R.id.recyclerView)
    private val fab: FloatingActionButton by bind(R.id.fab)
    private val drawerLayout: DrawerLayout by bind(R.id.drawerLayout)

    private lateinit var numberPickerDialog: NumberPickerDialog

    private lateinit var textBoxDialog: TextBoxDialog

    private lateinit var databaseHandler: DatabaseHandler

    private lateinit var teamRecyclerViewAdapter: TeamRecyclerViewAdapter

    private lateinit var navigationDrawerAdapter: NavigationDrawerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        databaseHandler = DatabaseHandler(application as TryCouchBaseApplication)

        setUpNavigationDrawer()

        setUpTeamsRecyclerView(databaseHandler.getFullTeamList())

        setUpTextBoxDialog()

        setUpNumberPickerDialog()

        fab.setOnClickListener {
            numberPickerDialog.show()
        }

    }

    private fun setUpTeamsRecyclerView(listOfTeams: List<Team>) {
        teamRecyclerViewAdapter = TeamRecyclerViewAdapter(listOfTeams as ArrayList<Team>)
        teamRecyclerViewAdapter.onItemClickListener = {
            it.selected = !it.selected
            databaseHandler.saveTeam(it)
        }

        teamRecyclerViewAdapter.onDeleteListener = {
            teamRecyclerViewAdapter.remove(it)
            databaseHandler.deleteTeam(it)
            navigationDrawerAdapter.updateRowsInfo()
        }

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = teamRecyclerViewAdapter
    }

    private fun setUpNavigationDrawer() {
        navigationDrawerAdapter = NavigationDrawerAdapter(databaseHandler)
        databaseHandler.addChangeListenerToUserInfoDocument(navigationDrawerAdapter.nameChangeListener)
        navigationDrawerAdapter.onUserInfoRequested = {
            databaseHandler.getUserInfo()
        }


        val drawerLayoutRecyclerView = findViewById(R.id.navigationDrawer) as RecyclerView
        drawerLayoutRecyclerView.layoutManager = LinearLayoutManager(this)
        drawerLayoutRecyclerView.adapter = navigationDrawerAdapter

        setHamburgerAndBackToolbarIcon()
    }

    private fun setHamburgerAndBackToolbarIcon() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        val drawerToggle = ActionBarDrawerToggle(this, drawerLayout, R.string.cd_drawer_open, R.string.cd_drawer_close)
        drawerToggle.isDrawerIndicatorEnabled = true
        drawerToggle.syncState()

        drawerLayout.addDrawerListener(drawerToggle)
    }

    private fun setUpNumberPickerDialog() {
        numberPickerDialog = NumberPickerDialog(this)
        numberPickerDialog.onNumberPicked = { numberPicked ->

            Toast.makeText(this@MainActivity, "Creating $numberPicked items...", Toast.LENGTH_SHORT).show()

            doAsync {
                // This takes like less than 5% of the total time
                val newTeams = List (numberPicked) { TeamGenerator.generate() }

                // This takes like more than 95% of the total time
                databaseHandler.createNewDocuments(newTeams)

                // This takes like no time
                teamRecyclerViewAdapter.addAll(newTeams)

                uiThread {
                    // This takes like no time
                    teamRecyclerViewAdapter.notifyDataSetChanged()
                    Toast.makeText(this@MainActivity, "$numberPicked items successfully generated", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun setUpTextBoxDialog() {
        textBoxDialog = TextBoxDialog(this)

        textBoxDialog.onUserInfoRequired = {
            databaseHandler.getUserInfo()
        }

        textBoxDialog.onOkButtonClicked = {
            databaseHandler.setUserName(it)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START)
                } else {
                    drawerLayout.openDrawer(GravityCompat.START)
                }
                return true
            }
            R.id.action_delete_all -> {

                Toast.makeText(this@MainActivity, "Delete all documents...", Toast.LENGTH_SHORT).show()

                doAsync {
                    val elements = databaseHandler.size()
                    databaseHandler.deleteAllDocuments()
                    teamRecyclerViewAdapter.removeAll()

                    uiThread {
                        teamRecyclerViewAdapter.notifyDataSetChanged()
                        Toast.makeText(this@MainActivity, "$elements items successfully deleted", Toast.LENGTH_SHORT).show()
                    }
                }

                return true
            }
            R.id.action_filter_real -> {

                val startingTimeView = System.nanoTime()
                val numberOfTeams = databaseHandler.numberOfTeams
                val finishingTimeView = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startingTimeView)
                Log.d(TAG, "Number of total elements: $numberOfTeams in $finishingTimeView ms")

                val startingTimeView1 = System.nanoTime()
                val teamsWithName = databaseHandler.getTeamFilteredByName("real")
                val finishingTimeView1 = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startingTimeView1)
                Log.d(TAG, "Number of filtered elements: ${teamsWithName.size} in $finishingTimeView1 ms")

                return true
            }
            R.id.action_filter_royal -> {

                val startingTimeView = System.nanoTime()
                val numberOfTeams = databaseHandler.numberOfTeams
                val finishingTimeView = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startingTimeView)
                Log.d(TAG, "Number of total elements: $numberOfTeams in $finishingTimeView ms")

                val startingTimeView1 = System.nanoTime()
                val teamsWithName = databaseHandler.getTeamFilteredByName("royal")
                val finishingTimeView1 = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startingTimeView1)
                Log.d(TAG, "--- Number of filtered elements: ${teamsWithName.size} in $finishingTimeView1 ms")

                return true
            }
            R.id.action_edit_name -> {
                textBoxDialog.show()
                return true
            }
        }


        return super.onOptionsItemSelected(item)
    }

}
