package namics.tdojo.trycouchbase.view.custom_views

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.Button
import android.widget.EditText
import namics.tdojo.trycouchbase.R
import namics.tdojo.trycouchbase.data.UserInfo
import namics.tdojo.trycouchbase.view.bind

class TextBoxDialog : Dialog {

    constructor(context: Context) : super(context)

    @Suppress("unused")
    constructor(context: Context, themeResId: Int) : super(context, themeResId)

    @Suppress("unused")
    constructor(context: Context, cancelable: Boolean, cancelListener: DialogInterface.OnCancelListener?) : super(context, cancelable, cancelListener)

    var onOkButtonClicked: ((String) -> Unit)? = null

    var onUserInfoRequired: (() -> UserInfo?)? = null

    private val inputEditText: EditText by bind(R.id.textBoxEditText)

    init {
        val dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_text_box, null)
        addContentView(dialogView, ViewGroup.LayoutParams(MATCH_PARENT, WRAP_CONTENT))

        val editButton: Button by bind(R.id.textBoxEditButton)
        val cancelButton: Button by bind(R.id.textBoxCancelButton)

        editButton.setOnClickListener {
            val newName = inputEditText.text.toString()
            onOkButtonClicked?.invoke(newName)
            dismiss()
        }

        cancelButton.setOnClickListener {
            cancel()
        }

    }

    override fun show() {
        super.show()
        inputEditText.setText(onUserInfoRequired?.invoke()?.name)
    }
}
