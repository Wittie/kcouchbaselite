package namics.tdojo.trycouchbase.view

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import com.couchbase.lite.Document
import namics.tdojo.trycouchbase.R
import namics.tdojo.trycouchbase.data.UserInfo
import namics.tdojo.trycouchbase.data.database.DatabaseHandler


class NavigationDrawerAdapter(val dBHandler : DatabaseHandler) : RecyclerView.Adapter<RowHolder>() {

    private val defaultAvatar = R.drawable.ic_person_black_96dp

    private val info = listOf(R.string.navigation_drawer_total_elements,
            R.string.navigation_drawer_teams_start_with_real,
            R.string.navigation_drawer_item_3,
            R.string.navigation_drawer_item_4
    )

    var onUserInfoRequested: (() -> UserInfo?)? = null

    val nameChangeListener = Document.ChangeListener {
        notifyItemChanged(0)
    }

    fun updateRowsInfo() {
        notifyItemRangeChanged(1, info.size)
    }

    override fun getItemCount() = info.size + 1 // 'cos of the header

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RowHolder  =
            RowHolder(LayoutInflater.from(parent.context).inflate(viewType, parent, false))

    override fun onBindViewHolder(holder: RowHolder, position: Int) {
        if (getItemViewType(position) == android.R.layout.simple_list_item_1) {
            val index = position -1 // 'cos of the header
            holder.rowText = when (index) {
                0 -> "${holder.getString(info[index])}: ${dBHandler.numberOfTeams}"
                1 -> "${holder.getString(info[index])}: ${dBHandler.getTeamFilteredByName("royal").size}"
                else -> holder.getString(info[index])
            }

        } else {
            val userInfo = onUserInfoRequested?.invoke()
            holder.rowText = userInfo?.name ?: ""
            (holder.itemView.findViewById(R.id.navigation_header_avatar) as ImageView?)?.setImageResource(userInfo?.imageResId ?: defaultAvatar)
        }
    }

    override fun getItemViewType(position: Int) =
            if (position != 0) android.R.layout.simple_list_item_1 else R.layout.navigation_drawer_header

}
