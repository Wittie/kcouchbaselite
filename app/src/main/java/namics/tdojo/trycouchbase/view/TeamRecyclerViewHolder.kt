package namics.tdojo.trycouchbase.view

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import namics.tdojo.trycouchbase.R
import namics.tdojo.trycouchbase.data.Team

class TeamRecyclerViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val nameTextView: TextView by bind(R.id.nameTextView)
    private val dateTextView: TextView by bind(R.id.dateTextView)
    private val championsTextView: TextView by bind(R.id.championsNumberTextView)
    private val deleteImageButton: ImageButton by bind(R.id.deleteImageButton)

    fun setTeam(position: Int, team: Team, onClickListener: ((Team) -> Unit)?, onDeleteListener: ((Team) -> Unit)?) {
        nameTextView.text = "$position. ${team.name}"
        dateTextView.text = formatStringRes(R.string.founded_at, team.foundedAtString, team.championsLeague)
        championsTextView.text = String.format ("%s", team.championsLeague)
        itemView.isSelected = team.selected
        itemView.setOnClickListener {
            onClickListener?.invoke(team)
            itemView.isSelected = !itemView.isSelected
            deleteImageButton.visibility = makeViewVisibleIf(team.selected)
        }

        deleteImageButton.visibility = makeViewVisibleIf(team.selected)
        deleteImageButton.setOnClickListener {
            onDeleteListener?.invoke(team)
        }
    }

    private fun makeViewVisibleIf(selected: Boolean) = if (selected) View.VISIBLE else View.INVISIBLE

}