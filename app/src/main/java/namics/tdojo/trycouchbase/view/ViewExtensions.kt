package namics.tdojo.trycouchbase.view

import android.app.Dialog
import android.support.annotation.IdRes
import android.support.annotation.StringRes
import android.support.v4.app.SupportActivity
import android.support.v7.widget.RecyclerView
import android.view.View
import java.util.*

fun RecyclerView.ViewHolder.formatStringRes(@StringRes stringRes : Int, vararg arg : Any ): String {
    return String.format(Locale.getDefault(), this.itemView.resources.getString(stringRes), *arg)
}


fun <T : View> Dialog.bind(@IdRes res : Int) : Lazy<T> {
    return viewBindImpl { this.findViewById(res) }
}

fun <T : View> RecyclerView.ViewHolder.bind(@IdRes res : Int) : Lazy<T> {
    return viewBindImpl { this.itemView.findViewById(res) }
}

fun <T : View> View.bind(@IdRes res : Int) : Lazy<T> {
    return viewBindImpl { this.findViewById(res) }
}

fun <T : View> SupportActivity.bind(@IdRes res : Int) : Lazy<T> {
    return viewBindImpl { this.findViewById(res) }
}

fun <T : View>viewBindImpl(getView: () -> View) : Lazy<T> {
    @Suppress("UNCHECKED_CAST")
    return lazy { getView() as T }
}