package namics.tdojo.trycouchbase.view

import android.graphics.Color
import android.support.annotation.StringRes
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView

class RowHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val textView: TextView by bind(android.R.id.text1)

    init {
        textView.setTextColor(Color.BLACK)
    }

    var rowText: String = ""
        set(value) {
            field = value
            textView.text = value
        }

    fun getString(@StringRes textRes: Int) : String {
        return itemView.context.getString(textRes)
    }

}
