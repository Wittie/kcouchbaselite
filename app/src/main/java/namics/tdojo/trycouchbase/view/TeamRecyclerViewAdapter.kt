package namics.tdojo.trycouchbase.view

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import namics.tdojo.trycouchbase.R
import namics.tdojo.trycouchbase.data.Team

class TeamRecyclerViewAdapter(val items: MutableList<Team>) : RecyclerView.Adapter<TeamRecyclerViewHolder>() {

    var onItemClickListener : ((Team) -> Unit)? = null
    var onDeleteListener: ((Team) -> Unit)? = null

    init {
        items.sort()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            TeamRecyclerViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.row_team, parent, false))

    override fun onBindViewHolder(holder: TeamRecyclerViewHolder, position: Int) = holder.setTeam(position, items[position], onItemClickListener, onDeleteListener)

    override fun getItemCount(): Int = items.size

    fun add(newTeam: Team) {
        items.add(newTeam)
        items.sort()

        val indexAdded = items.indexOf(newTeam)
        notifyItemInserted(indexAdded)
        for (i in indexAdded..items.size) {
            notifyItemChanged(i)
        }
    }

    fun addAll(newTeams : List<Team>) {
        items.addAll(newTeams)
    }

    fun remove(teamToDelete: Team) {
        val indexDeleted = items.indexOf(teamToDelete)
        if (indexDeleted >= 0) {
            items.remove(teamToDelete)
            notifyItemRemoved(indexDeleted)
            for (i in indexDeleted..items.size) {
                notifyItemChanged(i)
            }
        }
    }

    fun removeAll() {
        items.removeAll { true }
    }

}

