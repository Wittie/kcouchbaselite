package namics.tdojo.trycouchbase

import android.app.Application
import com.couchbase.lite.CouchbaseLiteException
import com.couchbase.lite.Database
import com.couchbase.lite.DatabaseOptions
import com.couchbase.lite.Manager
import com.couchbase.lite.android.AndroidContext
import java.io.IOException


class TryCouchBaseApplication : Application() {


    lateinit var database: Database

    override fun onCreate() {
        super.onCreate()

        // The db name must consist only of lowercase ASCII letters, digits, and
        // the special characters _$()+-/.
        // Also be < 240B
        // Also start with a lower case letter.
        openDatabase("data_base_name")
    }

    private fun openDatabase(dbName : String) {
        val options = DatabaseOptions()
        options.isCreate = true

        try {
            val manager = Manager(AndroidContext(applicationContext), Manager.DEFAULT_OPTIONS)
            database = manager.openDatabase(dbName, options)
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: CouchbaseLiteException) {
            e.printStackTrace()
        }
    }

}